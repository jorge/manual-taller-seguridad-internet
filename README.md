# manual-taller-seguridad-internet

Manuales y materiales para impartir talleres sobre seguridad en internet.

## ideas y dudas

- ¿que es seguridad?
- seguridad en la comunicacion
- seguridad guardando datos

## herramientas

### carteleria
- [cateles apps y software](https://git.sindominio.net/jorge/carteles-apps-software) para los talleres presenciales

### enlaces de interes
- Manuales del colectivo Boum https://guide.boum.org/
- Manuales del colectivo Boum traducidas al italiano https://git.sindominio.net/sindominio/manual-taller-seguridad-internet.git
- Diapositivas del colectivo Cisti. https://facciamo.cisti.org/#/ y su git repo https://git.lattuga.net/cisti/facciamo

## conclusiones
(todavía es pronto)